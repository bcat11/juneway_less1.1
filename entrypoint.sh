#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    # если база еще не запущена
    echo "=== Postgres еще не запущен ==="

    # Проверяем доступность хоста и порта
    while ! nc -z $POSTGRES_HOST $POSTGRES_PORT; do
      sleep 1
    done

    echo "=== Postgres запущен ==="
fi
# Удаляем все старые данные
python manage.py flush --noinput
# Выполняем миграции
python manage.py makemigrations
python manage.py migrate --fake-initial

exec "$@"
